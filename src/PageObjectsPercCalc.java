import openqa.selenium.*;
 
public class PageObjectsPercCalc 
{
   private static WebElement element = null;
 
   // Math Calc Link
   public static WebElement lnk_math_calc(WebDriver driver)
   {
      element = driver.findElement(By.xpath(".//*[@id='hl3']/li[1]/a"));
      return element;
   }
	
   //Percentage Calc Link
   public static WebElement lnk_percent_calc(WebDriver driver)
   {
      element = driver.findElement(By.xpath(".//*[@id='sciout']/div[3]/div[3]/span[1]"));
      return element;
   }
	
   public static WebElement lnk_percent_calc2(WebDriver driver)
   {
      element = driver.findElement(By.xpath(".//*[@id='occontent']/a[3]"));
      return element;
   }
   
   //Number 1 Text Box
   public static WebElement txt_num_1(WebDriver driver)
   {
      element = driver.findElement(By.id("cpar1"));
      return element;
   }
	
   //Number 2 Text Box	
   public static WebElement txt_num_2(WebDriver driver)
   {
      element = driver.findElement(By.id("cpar2"));
      return element;
   }
	
   //Calculate Button	
   public static WebElement btn_calc(WebDriver driver)
   {
      element = driver.findElement(By.xpath(".//*[@id='content']/table[1]/tbody/tr[2]/td/input[2]"));
      return element;
   }	
	
   // Result	
   public static WebElement web_result(WebDriver driver)
   {
      element = driver.findElement(By.xpath(".//*[@id='content']/p[2]/font/b"));
      return element;
   }	
}
