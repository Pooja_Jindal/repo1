import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class all_link {

	public static void main(String[] args)  {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver","C:\\seleniumweb\\geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver(); //Opens FF browser
		
		//driver.manage().timeouts().implicitlyWait(10000,TimeUnit.SECONDS); 
		
		driver.manage().window().maximize();
		driver.get("https://www.google.com/");		
		
		driver.findElement(By.name("q")).sendKeys("Testing");
		driver.findElement(By.name("q")).submit();

        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<WebElement> links =  driver.findElements(By.tagName("a"));		
	
		System.out.println("Number of Links in the Page is " + links.size());
		
		for (int i = 0; i<links.size(); i=i+1) {
	
			System.out.println("Name of Link#  " +i+"    "  + links.get(i).getText());
	 }
		
  }

}
