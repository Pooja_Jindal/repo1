import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class flight {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver","C:\\seleniumweb\\geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver(); //Opens FF browser
		driver.get("http://newtours.demoaut.com/");
		driver.findElement(By.linkText("REGISTER")).click();
		driver.findElement(By.cssSelector("input[name=\"register\"]")).click();
		driver.findElement(By.linkText("Flights")).click();		
		driver.findElement(By.cssSelector("input[value=\"roundtrip\"]")).click();
		
		Select dropdown=new Select(driver.findElement(By.cssSelector("select[name=\"passCount\"]")));
		dropdown.selectByVisibleText("4");
		
		Select dropdown1=new Select(driver.findElement(By.cssSelector("select[name=\"fromPort\"]")));
		dropdown1.selectByVisibleText("San Francisco");
		
		Select dropdown2=new Select(driver.findElement(By.cssSelector("select[name=\"fromMonth\"]")));
		dropdown2.selectByVisibleText("May");
		Select dropdown3=new Select(driver.findElement(By.cssSelector("select[name=\"fromDay\"]")));
		dropdown3.selectByVisibleText("2");
		Select dropdown4=new Select(driver.findElement(By.cssSelector("select[name=\"toPort\"]")));
		dropdown4.selectByVisibleText("New York");
		Select dropdown5=new Select(driver.findElement(By.cssSelector("select[name=\"toMonth\"]")));
		dropdown5.selectByVisibleText("June");
		Select dropdown6=new Select(driver.findElement(By.cssSelector("select[name=\"toDay\"]")));
		dropdown6.selectByVisibleText("3");
		driver.findElement(By.cssSelector("input[value=\"Business\"]")).click();
		Select dropdown7=new Select(driver.findElement(By.cssSelector("select[name=\"airline\"]")));
		dropdown7.selectByVisibleText("Blue Skies Airlines");
		
		driver.findElement(By.cssSelector("input[name=\"findFlights\"]")).click();
		
		
		

	}

}
